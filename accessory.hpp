#ifndef _ACCESSORY_H
#define _ACCESSORY_H 1

#include "config.hpp"

extern unsigned char x_map_size;
extern unsigned char y_map_size;
extern unsigned char z_map_size;
bool Rotate;
bool last_output = false;
unsigned int unitary_size;
game_map* Map;

void initialization()
{
    window_width = 640.0f;
    window_height = 480.0f;
    unitary_size = 5;
    old_spd = 700;
    unsigned char x_map_size = 5;
    unsigned char y_map_size = 5;
    unsigned char z_map_size = 10;
    Rotate = false;
    Map = new game_map(x_map_size, y_map_size, z_map_size);
    Map -> clear_map();
    Map -> set_speed(700);
}
void ProcessMenu(int value)
	{
	switch(value)
		{
        case 1:
            Rotate = !Rotate;
        break;
		case 2:
            system("clear");
			Map -> clear_map();
			if (Rotate)
			last_output = false;
			Rotate = false;
        break;
        case 3:
            exit(0);
        break;
		}

	glutPostRedisplay();
	}

void resize_window(GLsizei current_width, GLsizei current_height)
{
    GLfloat fAspect;

    if(current_height == 0)
        current_height = 1;

    // Set Viewport to window dimensions
    glViewport(0, 0, current_width, current_height);

    fAspect = (GLfloat)current_width / (GLfloat)current_height;

    // Reset coordinate system
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    // Produce the perspective projection
    gluPerspective(60.0f, fAspect, 1.0, 400.0);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void finalization()
{
    delete(Map);
}

#endif // _ACCESSORY_H
