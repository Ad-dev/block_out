#include "render.h"
#include "config.hpp"

int rot, hnd, old_spd;
void preparation()
{
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glEnable(GL_LINE_SMOOTH);
    glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);

}

void render()
{
    GLfloat x_map = Map->get_gl_x();
    GLfloat y_map = Map->get_gl_y();
    GLfloat z_map = Map->get_gl_z();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


    glPushMatrix();
    if (Rotate == true)
    {
        glTranslatef(0.0f, 50.0f, -90.0f);
        glRotatef(-80, 1.0f, 0.0f, 0.0f);
        glRotatef(rot, 0.0f, 0.0f, 1.0f);
        if (Map -> get_speed() != 55)
        old_spd = Map -> get_speed();
        Map -> set_speed(55);
        hnd = 10;
    }
    else
    {
        if (Map -> get_speed() == 55)
        Map -> set_speed(old_spd);
        hnd = 0;
    }

    glTranslatef((GLfloat)(-(unitary_size * x_map)/2), (GLfloat)(-(unitary_size * y_map)/2), (GLfloat)(-(unitary_size * z_map) * 1.435f));


    glMatrixMode(GL_MODELVIEW);
    glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////// opaque object ///
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    glEnable(GL_DEPTH_TEST);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);

////////////////////////////////////////////////////////////////
/////////////////////////////////////////////// map builder ///
//////////////////////////////////////////////////////////////
    glPushMatrix();

    glColor3f(0.5f, 0.5f, 0.5f);
    glLineWidth(2.0f);
    glBegin(GL_LINES);
    for (int i = 0; i <= x_map; i++)
    {
        glVertex3f((GLfloat)(unitary_size * i), 0.0f, 0.0f);
        glVertex3f((GLfloat)(unitary_size * i), (GLfloat)(unitary_size * y_map), 0.0f);

        glVertex3f((GLfloat)(unitary_size * i), 0.0f, 0.0f);
        glVertex3f((GLfloat)(unitary_size * i), 0.0f, (GLfloat)(unitary_size * z_map));

        glVertex3f((GLfloat)(unitary_size * i), (GLfloat)(unitary_size * y_map), 0.0f);
        glVertex3f((GLfloat)(unitary_size * i), (GLfloat)(unitary_size * y_map), (GLfloat)(unitary_size * z_map));
    }
    for (int i = 0; i <= y_map; i++)
    {
        glVertex3f(0.0f, (GLfloat)(unitary_size * i), 0.0f);
        glVertex3f((GLfloat)(unitary_size * x_map), (GLfloat)(unitary_size * i), 0.0f);

        glVertex3f(0.0f, (GLfloat)(unitary_size * i), 0.0f);
        glVertex3f(0.0f, (GLfloat)(unitary_size * i), (GLfloat)(unitary_size * z_map));

        glVertex3f((unitary_size * x_map), (GLfloat)(unitary_size * i), 0.0f);
        glVertex3f((unitary_size * x_map), (GLfloat)(unitary_size * i), (GLfloat)(unitary_size * z_map));
        for (int j = 0; j <= z_map; j++)
        {

        }
    }
    for (int i = 1; i <= z_map; i++)
    {
        glVertex3f(0.0f, 0.0f, (GLfloat)(unitary_size * i));
        glVertex3f(0.0f, (GLfloat)(unitary_size * y_map), (GLfloat)(unitary_size * i));

        glVertex3f(0.0f, 0.0f, (GLfloat)(unitary_size * i));
        glVertex3f((GLfloat)(unitary_size * x_map), 0.0f, (GLfloat)(unitary_size * i));

        glVertex3f((GLfloat)(unitary_size * x_map), 0.0f, (GLfloat)(unitary_size * i));
        glVertex3f((GLfloat)(unitary_size * x_map), (GLfloat)(unitary_size * y_map), (GLfloat)(unitary_size * i));

        glVertex3f((GLfloat)(unitary_size * x_map), (GLfloat)(unitary_size * y_map), (GLfloat)(unitary_size * i));
        glVertex3f(0.0f, (GLfloat)(unitary_size * y_map), (GLfloat)(unitary_size * i));

    }
    glEnd();

////////////////////////////////////////////////////////////////
////////////////////////////////////////// geometry builder ///
//////////////////////////////////////////////////////////////
    glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
    glEnable(GL_CULL_FACE);
    glLineWidth(2.0f);
    for (unsigned char i = 0; i < x_map; i++)
    {
        for (unsigned char j = 0; j < y_map; j++)
        {
            for (unsigned char k = 0; k < z_map; k++)
            {
                if (Map->get_cell_info(i,j,k) == 1)
                {
                    switch (k)
                    {
                        case 0:
                            glColor3f(1.0f, 0.0f, 0.0f);
                        break;
                        case 1:
                            glColor3f(0.0f, 1.0f, 0.0f);
                        break;
                        case 2:
                            glColor3f(0.0f, 0.0f, 1.0f);
                        break;
                        case 3:
                            glColor3f(1.0f, 1.0f, 0.0f);
                        break;
                        case 4:
                            glColor3f(0.0f, 1.0f, 1.0f);
                        break;
                        case 5:
                            glColor3f(1.0f, 0.0f, 1.0f);
                        break;
                        case 6:
                            glColor3f(5.0f, 0.0f, 0.0f);
                        break;
                        case 7:
                            glColor3f(0.0f, 0.5f, 0.0f);
                        break;
                        case 8:
                            glColor3f(0.0f, 0.0f, 0.5f);
                        break;
                        case 9:
                            glColor3f(0.5f, 0.0f, 0.5f);
                        break;

                    }
                    glBegin(GL_QUADS);
                    //front
                    glVertex3f((GLfloat)(unitary_size * i), (GLfloat)(unitary_size * j), (GLfloat)(unitary_size * (k + 1)));
                    glVertex3f((GLfloat)(unitary_size * (i + 1)), (GLfloat)(unitary_size * j), (GLfloat)(unitary_size * (k + 1)));
                    glVertex3f((GLfloat)(unitary_size * (i + 1)), (GLfloat)(unitary_size * (j + 1)), (GLfloat)(unitary_size * (k + 1)));
                    glVertex3f((GLfloat)(unitary_size * i), (GLfloat)(unitary_size * (j + 1)), (GLfloat)(unitary_size * (k + 1)));

                    //bottom
                    glVertex3f((GLfloat)(unitary_size * i), (GLfloat)(unitary_size * j), (GLfloat)(unitary_size * k));
                    glVertex3f((GLfloat)(unitary_size * (i + 1)), (GLfloat)(unitary_size * j), (GLfloat)(unitary_size * k));
                    glVertex3f((GLfloat)(unitary_size * (i + 1)), (GLfloat)(unitary_size * j), (GLfloat)(unitary_size * (k + 1)));
                    glVertex3f((GLfloat)(unitary_size * i), (GLfloat)(unitary_size * j), (GLfloat)(unitary_size * (k + 1)));

                    //left
                    glVertex3f((GLfloat)(unitary_size * i), (GLfloat)(unitary_size * (j + 1)), (GLfloat)(unitary_size * k));
                    glVertex3f((GLfloat)(unitary_size * i), (GLfloat)(unitary_size * j), (GLfloat)(unitary_size * k));
                    glVertex3f((GLfloat)(unitary_size * i), (GLfloat)(unitary_size * j), (GLfloat)(unitary_size * (k + 1)));
                    glVertex3f((GLfloat)(unitary_size * i), (GLfloat)(unitary_size * (j + 1)), (GLfloat)(unitary_size * (k + 1)));

                    //right
                    glVertex3f((GLfloat)(unitary_size * (i + 1)), (GLfloat)(unitary_size * (j + 1)), (GLfloat)(unitary_size * (k + 1)));
                    glVertex3f((GLfloat)(unitary_size * (i + 1)), (GLfloat)(unitary_size * j), (GLfloat)(unitary_size * (k + 1)));
                    glVertex3f((GLfloat)(unitary_size * (i + 1)), (GLfloat)(unitary_size * j), (GLfloat)(unitary_size * k));
                    glVertex3f((GLfloat)(unitary_size * (i + 1)), (GLfloat)(unitary_size * (j + 1)), (GLfloat)(unitary_size * k));

                    //top
                    glVertex3f((GLfloat)(unitary_size * i), (GLfloat)(unitary_size * (j + 1)), (GLfloat)(unitary_size * (k + 1)));
                    glVertex3f((GLfloat)(unitary_size * (i + 1)), (GLfloat)(unitary_size * (j + 1)), (GLfloat)(unitary_size * (k + 1)));
                    glVertex3f((GLfloat)(unitary_size * (i + 1)), (GLfloat)(unitary_size * (j + 1)), (GLfloat)(unitary_size * k));
                    glVertex3f((GLfloat)(unitary_size * i), (GLfloat)(unitary_size * (j + 1)), (GLfloat)(unitary_size * k));
                    glEnd();

                    glColor3f(0.0f, 0.0f, 0.0f);
                    glBegin(GL_LINE_LOOP);
                    //front
                    glVertex3f((GLfloat)(unitary_size * i), (GLfloat)(unitary_size * j), (GLfloat)(unitary_size * (k + 1)));
                    glVertex3f((GLfloat)(unitary_size * (i + 1)), (GLfloat)(unitary_size * j), (GLfloat)(unitary_size * (k + 1)));
                    glVertex3f((GLfloat)(unitary_size * (i + 1)), (GLfloat)(unitary_size * (j + 1)), (GLfloat)(unitary_size * (k + 1)));
                    glVertex3f((GLfloat)(unitary_size * i), (GLfloat)(unitary_size * (j + 1)), (GLfloat)(unitary_size * (k + 1)));
                    glEnd();

                    glBegin(GL_LINE_LOOP);
                    //bottom
                    glVertex3f((GLfloat)(unitary_size * i), (GLfloat)(unitary_size * j), (GLfloat)(unitary_size * k));
                    glVertex3f((GLfloat)(unitary_size * (i + 1)), (GLfloat)(unitary_size * j), (GLfloat)(unitary_size * k));
                    glVertex3f((GLfloat)(unitary_size * (i + 1)), (GLfloat)(unitary_size * j), (GLfloat)(unitary_size * (k + 1)));
                    glVertex3f((GLfloat)(unitary_size * i), (GLfloat)(unitary_size * j), (GLfloat)(unitary_size * (k + 1)));
                    glEnd();

                    glBegin(GL_LINE_LOOP);
                    //left
                    glVertex3f((GLfloat)(unitary_size * i), (GLfloat)(unitary_size * (j + 1)), (GLfloat)(unitary_size * k));
                    glVertex3f((GLfloat)(unitary_size * i), (GLfloat)(unitary_size * j), (GLfloat)(unitary_size * k));
                    glVertex3f((GLfloat)(unitary_size * i), (GLfloat)(unitary_size * j), (GLfloat)(unitary_size * (k + 1)));
                    glVertex3f((GLfloat)(unitary_size * i), (GLfloat)(unitary_size * (j + 1)), (GLfloat)(unitary_size * (k + 1)));
                    glEnd();

                    glBegin(GL_LINE_LOOP);
                    //right
                    glVertex3f((GLfloat)(unitary_size * (i + 1)), (GLfloat)(unitary_size * (j + 1)), (GLfloat)(unitary_size * (k + 1)));
                    glVertex3f((GLfloat)(unitary_size * (i + 1)), (GLfloat)(unitary_size * j), (GLfloat)(unitary_size * (k + 1)));
                    glVertex3f((GLfloat)(unitary_size * (i + 1)), (GLfloat)(unitary_size * j), (GLfloat)(unitary_size * k));
                    glVertex3f((GLfloat)(unitary_size * (i + 1)), (GLfloat)(unitary_size * (j + 1)), (GLfloat)(unitary_size * k));
                    glEnd();

                    glBegin(GL_LINE_LOOP);
                    //top
                    glVertex3f((GLfloat)(unitary_size * i), (GLfloat)(unitary_size * (j + 1)), (GLfloat)(unitary_size * (k + 1)));
                    glVertex3f((GLfloat)(unitary_size * (i + 1)), (GLfloat)(unitary_size * (j + 1)), (GLfloat)(unitary_size * (k + 1)));
                    glVertex3f((GLfloat)(unitary_size * (i + 1)), (GLfloat)(unitary_size * (j + 1)), (GLfloat)(unitary_size * k));
                    glVertex3f((GLfloat)(unitary_size * i), (GLfloat)(unitary_size * (j + 1)), (GLfloat)(unitary_size * k));
                    glEnd();
                }
            }
        }
    }

    glDisable(GL_CULL_FACE);

    glPopMatrix();
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////// transparent objects ///
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    glDisable(GL_DEPTH_TEST);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    for (unsigned char i = 0; i < x_map; i++)
    {
        for (unsigned char j = 0; j < y_map; j++)
        {
            for (unsigned char k = 0; k < z_map; k++)
            {
                if (Map->get_cell_info(i,j,k) == 2)
                {
                    glColor4f(0.0f, 1.0f, 0.0f, 0.3f);
                    glBegin(GL_QUADS);
                    //front
                    glVertex3f((GLfloat)(unitary_size * i), (GLfloat)(unitary_size * j), (GLfloat)(unitary_size * (k + 1)));
                    glVertex3f((GLfloat)(unitary_size * (i + 1)), (GLfloat)(unitary_size * j), (GLfloat)(unitary_size * (k + 1)));
                    glVertex3f((GLfloat)(unitary_size * (i + 1)), (GLfloat)(unitary_size * (j + 1)), (GLfloat)(unitary_size * (k + 1)));
                    glVertex3f((GLfloat)(unitary_size * i), (GLfloat)(unitary_size * (j + 1)), (GLfloat)(unitary_size * (k + 1)));

                    //bottom
                    glVertex3f((GLfloat)(unitary_size * i), (GLfloat)(unitary_size * j), (GLfloat)(unitary_size * k));
                    glVertex3f((GLfloat)(unitary_size * (i + 1)), (GLfloat)(unitary_size * j), (GLfloat)(unitary_size * k));
                    glVertex3f((GLfloat)(unitary_size * (i + 1)), (GLfloat)(unitary_size * j), (GLfloat)(unitary_size * (k + 1)));
                    glVertex3f((GLfloat)(unitary_size * i), (GLfloat)(unitary_size * j), (GLfloat)(unitary_size * (k + 1)));

                    //left
                    glVertex3f((GLfloat)(unitary_size * i), (GLfloat)(unitary_size * (j + 1)), (GLfloat)(unitary_size * k));
                    glVertex3f((GLfloat)(unitary_size * i), (GLfloat)(unitary_size * j), (GLfloat)(unitary_size * k));
                    glVertex3f((GLfloat)(unitary_size * i), (GLfloat)(unitary_size * j), (GLfloat)(unitary_size * (k + 1)));
                    glVertex3f((GLfloat)(unitary_size * i), (GLfloat)(unitary_size * (j + 1)), (GLfloat)(unitary_size * (k + 1)));

                    //right
                    glVertex3f((GLfloat)(unitary_size * (i + 1)), (GLfloat)(unitary_size * (j + 1)), (GLfloat)(unitary_size * (k + 1)));
                    glVertex3f((GLfloat)(unitary_size * (i + 1)), (GLfloat)(unitary_size * j), (GLfloat)(unitary_size * (k + 1)));
                    glVertex3f((GLfloat)(unitary_size * (i + 1)), (GLfloat)(unitary_size * j), (GLfloat)(unitary_size * k));
                    glVertex3f((GLfloat)(unitary_size * (i + 1)), (GLfloat)(unitary_size * (j + 1)), (GLfloat)(unitary_size * k));

                    //top
                    glVertex3f((GLfloat)(unitary_size * i), (GLfloat)(unitary_size * (j + 1)), (GLfloat)(unitary_size * (k + 1)));
                    glVertex3f((GLfloat)(unitary_size * (i + 1)), (GLfloat)(unitary_size * (j + 1)), (GLfloat)(unitary_size * (k + 1)));
                    glVertex3f((GLfloat)(unitary_size * (i + 1)), (GLfloat)(unitary_size * (j + 1)), (GLfloat)(unitary_size * k));
                    glVertex3f((GLfloat)(unitary_size * i), (GLfloat)(unitary_size * (j + 1)), (GLfloat)(unitary_size * k));
                    glEnd();

                    glColor4f(0.0f, 0.0f, 0.0f, 0.5f);
                    glBegin(GL_LINE_LOOP);
                    //front
                    glVertex3f((GLfloat)(unitary_size * i), (GLfloat)(unitary_size * j), (GLfloat)(unitary_size * (k + 1)));
                    glVertex3f((GLfloat)(unitary_size * (i + 1)), (GLfloat)(unitary_size * j), (GLfloat)(unitary_size * (k + 1)));
                    glVertex3f((GLfloat)(unitary_size * (i + 1)), (GLfloat)(unitary_size * (j + 1)), (GLfloat)(unitary_size * (k + 1)));
                    glVertex3f((GLfloat)(unitary_size * i), (GLfloat)(unitary_size * (j + 1)), (GLfloat)(unitary_size * (k + 1)));
                    glEnd();

                    glBegin(GL_LINE_LOOP);
                    //bottom
                    glVertex3f((GLfloat)(unitary_size * i), (GLfloat)(unitary_size * j), (GLfloat)(unitary_size * k));
                    glVertex3f((GLfloat)(unitary_size * (i + 1)), (GLfloat)(unitary_size * j), (GLfloat)(unitary_size * k));
                    glVertex3f((GLfloat)(unitary_size * (i + 1)), (GLfloat)(unitary_size * j), (GLfloat)(unitary_size * (k + 1)));
                    glVertex3f((GLfloat)(unitary_size * i), (GLfloat)(unitary_size * j), (GLfloat)(unitary_size * (k + 1)));
                    glEnd();

                    glBegin(GL_LINE_LOOP);
                    //left
                    glVertex3f((GLfloat)(unitary_size * i), (GLfloat)(unitary_size * (j + 1)), (GLfloat)(unitary_size * k));
                    glVertex3f((GLfloat)(unitary_size * i), (GLfloat)(unitary_size * j), (GLfloat)(unitary_size * k));
                    glVertex3f((GLfloat)(unitary_size * i), (GLfloat)(unitary_size * j), (GLfloat)(unitary_size * (k + 1)));
                    glVertex3f((GLfloat)(unitary_size * i), (GLfloat)(unitary_size * (j + 1)), (GLfloat)(unitary_size * (k + 1)));
                    glEnd();

                    glBegin(GL_LINE_LOOP);
                    //right
                    glVertex3f((GLfloat)(unitary_size * (i + 1)), (GLfloat)(unitary_size * (j + 1)), (GLfloat)(unitary_size * (k + 1)));
                    glVertex3f((GLfloat)(unitary_size * (i + 1)), (GLfloat)(unitary_size * j), (GLfloat)(unitary_size * (k + 1)));
                    glVertex3f((GLfloat)(unitary_size * (i + 1)), (GLfloat)(unitary_size * j), (GLfloat)(unitary_size * k));
                    glVertex3f((GLfloat)(unitary_size * (i + 1)), (GLfloat)(unitary_size * (j + 1)), (GLfloat)(unitary_size * k));
                    glEnd();

                    glBegin(GL_LINE_LOOP);
                    //top
                    glVertex3f((GLfloat)(unitary_size * i), (GLfloat)(unitary_size * (j + 1)), (GLfloat)(unitary_size * (k + 1)));
                    glVertex3f((GLfloat)(unitary_size * (i + 1)), (GLfloat)(unitary_size * (j + 1)), (GLfloat)(unitary_size * (k + 1)));
                    glVertex3f((GLfloat)(unitary_size * (i + 1)), (GLfloat)(unitary_size * (j + 1)), (GLfloat)(unitary_size * k));
                    glVertex3f((GLfloat)(unitary_size * i), (GLfloat)(unitary_size * (j + 1)), (GLfloat)(unitary_size * k));
                    glEnd();
                }
            }
        }
    }
    glDisable(GL_BLEND);



    glPopMatrix();

    glutSwapBuffers();
}


void timer(int value)
{
    rot += 1;
    if (rot > 360)
        rot = 0;
    Map -> data_handler(hnd);
    glutPostRedisplay();
    if (Map -> get_speed() > 0)
    glutTimerFunc(Map -> get_speed(), timer, 1);
    else if ((Map -> get_speed() == -1) && !last_output)
    {
        std::cout << "game over!" << std::endl;
        last_output = true;
        Rotate = true;
    }
    else if ((Map -> get_speed() == -2) && !last_output)
    {
        std::cout << "Congratulations!!! You won : )" << std::endl;
        last_output = true;
        Rotate = true;
    }

    if (Map -> get_speed() < 0)
    {
        glutTimerFunc(10, timer, 1);
    }
}
