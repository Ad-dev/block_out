#ifndef _CONFIG_H
#define _CONFIG_H 1

#include <stdlib.h>
#include <iostream>
#include <ctime>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/freeglut.h>

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////// structures & classes ///
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class game_map
{
private:
    unsigned char ***_size_map;
    unsigned char ***tmp_map;
    unsigned char _x_size;
    unsigned char _y_size;
    unsigned char _z_size;
    int _speed;
    unsigned int score;

public:
////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////// methods ///
//////////////////////////////////////////////////////////////
    void generator()
    {
        int r = rand() % 3 + 1;
        switch (rand() % 7)
        {
        case 0:
        case 2:
        case 4:
            for (int x = 0; x < r; x++)
            {
                if (tmp_map[x + (_x_size / 2 - r / 2)][_y_size / 2 - r / 2 + 1][_z_size - 1] != 1)
                    tmp_map[x + (_x_size / 2 - r / 2)][_y_size / 2 - r / 2 + 1][_z_size - 1] = 2;
            }
            break;
        case 1:
        case 3:
        case 5:
            for (int y = 0; y < r; y++)
            {
                if (tmp_map[_x_size / 2 - r / 2 + 1][y + (_y_size / 2 - r / 2)][_z_size - 1] != 1)
                    tmp_map[_x_size / 2 - r / 2 + 1][y + (_y_size / 2 - r / 2)][_z_size - 1] = 2;
            }

            break;
        case 6:
            for (int z = _z_size - 1; z > _z_size - (r + 1); z--)
            {
                if (tmp_map[_x_size / 2][_y_size / 2][z] != 1)
                    tmp_map[_x_size / 2][_y_size / 2][z] = 2;
            }
            break;

        }
        this -> set_speed(700);
    }

    void data_handler(unsigned char comand)
    {
        for (int x = 0; x < _x_size; x++)
        {
            for (int y = 0; y < _y_size; y++)
            {
                for (int z = 0; z < _z_size; z++)
                {
                    tmp_map[x][y][z] = _size_map[x][y][z];
                }
            }
        }
        int check_count = 0;
        bool check = true;
        bool can_move = true;
        switch (comand)
        {
        case 0:
            for (int x = 0; x < _x_size; x++)
            {
                for (int y = 0; y < _y_size; y++)
                {
                    for (int z = 0; z < _z_size; z++)
                    {
                        if ((tmp_map[x][y][z] == 2) && ((z == 0) || (tmp_map[x][y][z-1] == 1)))
                            can_move = false;
                    }
                }
            }
            for (int i = 0; i < _x_size; i++)
            {
                for (int j = 0; j < _y_size; j++)
                {
                    for (int k = 0; k < _z_size; k++)
                    {
                        if ((tmp_map[i][j][k] == 2) && ((k != 0) && (tmp_map[i][j][k-1] != 1)) && can_move)
                        {
                            tmp_map[i][j][k] = 0;
                            tmp_map[i][j][k - 1] = 2;
                        }
                        if ((tmp_map[i][j][k] == 2) && ((k == 0) || (tmp_map[i][j][k-1] == 1)))
                        {
                            for (int x = 0; x < _x_size; x++)
                            {
                                for (int y = 0; y < _y_size; y++)
                                {
                                    for (int z = 0; z < _z_size; z++)
                                    {
                                        if (tmp_map[x][y][z] == 2)
                                            tmp_map[x][y][z] = 1;
                                    }
                                }
                            }
                            for (int z = 0; z < _z_size - 1; z++)
                            {
                                check = false;
                                check_count = 0;
                                for (int y = 0; y < _y_size; y++)
                                {
                                    for (int x = 0; x < _x_size; x++)
                                    {
                                        if (tmp_map[x][y][z] != 1)
                                        {
                                            check_count = 0;
                                            check = true;
                                        }
                                        else
                                            check_count++;
                                    }
                                }
                                if (check_count == _x_size * _y_size)
                                {
                                    std::cout << "DEBUG | check_count: " << check_count << std::endl;
                                    for (int sub_z = z; sub_z < _z_size - 1; sub_z++)
                                    {
                                        for (int y = 0; y < _y_size; y++)
                                        {
                                            for (int x = 0; x < _x_size; x++)
                                            {
                                                tmp_map[x][y][sub_z] = tmp_map[x][y][sub_z + 1];
                                            }
                                        }
                                    }
                                    check_count = 0;
                                    system("clear");
                                    score += 100;
                                    std::cout << "Your score: " << score << " (+100)"<< std::endl;
                                    if (score == 10000)
                                        this -> set_speed(-2);
                                }
                                /*if (!check)
                                {
                                    for (int z = 0; z < _z_size - 1; z++)
                                    {
                                        for (int y = 0; y < _y_size; y++)
                                        {
                                            for (int x = 0; x < _x_size; x++)
                                            {
                                                tmp_map[x][y][z] = tmp_map[x][y][z + 1];
                                            }
                                        }
                                    }
                                    system("clear");
                                    score += 100;
                                    std::cout << "Your score: " << score << " (+100)"<< std::endl;
                                    if (score == 10000)
                                        this -> set_speed(-2);
                                }*/
                            }
                            if (!check)
                            {
                                for (int y = 0; y < _y_size; y++)
                                {
                                    for (int x = 0; x < _x_size; x++)
                                    {
                                        tmp_map[x][y][_z_size - 1] = 0;
                                    }
                                }
                                check = true;
                            }
                        }
                    }
                }
            }
            for (int i = 0; i < _x_size; i++)
            {
                for (int j = 0; j < _y_size; j++)
                {
                    for (int k = 0; k < _z_size; k++)
                    {

                        if ((tmp_map[i][j][k] == 2) && ((k == 0) || (tmp_map[i][j][k-1] == 1)))
                            this -> set_speed(500);
                        if (tmp_map[i][j][_z_size - 1] == 1)
                            this -> set_speed(-1);
                        if (tmp_map[i][j][k] == 2)
                            check = false;
                    }
                }
            }
            if (check && (this -> get_speed() > 0))
                this -> generator();
            else
                check = true;
            break;

        case 1:
            for (int k = 0; k < _z_size; k++)
            {
                for (int i = 0; i < _x_size; i++)
                {

                    if (_size_map[i][_y_size - 1][k] == 2)
                        check = false;
                }
            }

            if (check)
                for (int k = 0; k < _z_size; k++)
                {
                    for (int i = _x_size - 1; i >= 0; i--)
                    {
                        for (int j = _y_size - 2; j >= 0; j--)
                        {
                            if (_size_map[i][j][k] == 2)
                            {
                                tmp_map[i][j][k] = 0;
                                tmp_map[i][j + 1][k] += 2;
                            }
                        }
                    }
                }
            break;

        case 2:
            for (int k = 0; k < _z_size; k++)
            {
                for (int j = 0; j < _y_size; j++)
                {

                    if (_size_map[_x_size - 1][j][k] == 2)
                        check = false;
                }
            }

            if (check)
                for (int k = 0; k < _z_size; k++)
                {
                    for (int j = _y_size - 1; j >= 0; j--)
                    {
                        for (int i = _x_size - 2; i >= 0; i--)
                        {
                            if (_size_map[i][j][k] == 2)
                            {
                                tmp_map[i][j][k] = 0;
                                tmp_map[i + 1][j][k] += 2;
                            }
                        }
                    }
                }
            break;

        case 3:
            for (int k = 0; k < _z_size; k++)
            {
                for (int i = _x_size - 1; i >= 0; i--)
                {
                    if (_size_map[i][0][k] == 2)
                        check = false;
                }
            }

            if (check)
                for (int k = 0; k < _z_size; k++)
                {
                    for (int i = 0; i < _x_size; i++)
                    {
                        for (int j = 1; j < _y_size; j++)
                        {
                            if (_size_map[i][j][k] == 2)
                            {
                                tmp_map[i][j][k] = 0;
                                tmp_map[i][j - 1][k] += 2;
                            }
                        }
                    }
                }
            break;

        case 4:
            for (int k = 0; k < _z_size; k++)
            {
                for (int j = 0; j < _y_size; j++)
                {
                    if (_size_map[0][j][k] == 2)
                        check = false;
                }
            }

            if (check)
                for (int k = 0; k < _z_size; k++)
                {
                    for (int i = 1; i < _x_size; i++)
                    {
                        for (int j = 0; j < _y_size; j++)
                        {
                            if (_size_map[i][j][k] == 2)
                            {
                                tmp_map[i][j][k] = 0;
                                tmp_map[i - 1][j][k] += 2;
                            }
                        }
                    }
                }
            break;
        }

        check = true;
        for (int x = 0; x < _x_size; x++)
        {
            for (int y = 0; y < _y_size; y++)
            {
                for (int z = 0; z < _z_size; z++)
                {
                    if (tmp_map[x][y][z] == 3)
                        check = false;
                }
            }
        }
        if (check)
            for (int x = 0; x < _x_size; x++)
            {
                for (int y = 0; y < _y_size; y++)
                {
                    for (int z = 0; z < _z_size; z++)
                    {
                        _size_map[x][y][z] = tmp_map[x][y][z];
                    }
                }
            }
    }

    GLfloat get_gl_x()
    {
        return (GLfloat)_x_size;
    }

    GLfloat get_gl_y()
    {
        return (GLfloat)_y_size;
    }

    GLfloat get_gl_z()
    {
        return (GLfloat)_z_size;
    }

    void get_all_sizes(unsigned char* x_size, unsigned char* y_size, unsigned char* z_size)
    {
        if (x_size != nullptr)
            *x_size = _x_size;
        if (y_size != nullptr)
            *y_size = _y_size;
        if (z_size != nullptr)
            *z_size = _z_size;
    }

    unsigned char get_cell_info(unsigned char x_pos, unsigned char y_pos, unsigned char z_pos)
    {
        return(_size_map[x_pos][y_pos][z_pos]);
    }

    void clear_map()
    {
        for (unsigned char i = 0; i < _x_size; i++)
        {
            for (unsigned char j = 0; j < _y_size; j++)
            {
                for (unsigned char k = 0; k < _z_size; k++)
                {
                    _size_map[i][j][k] = 0;
                }
            }
        }
        score = 0;
        std::cout << "Your score: " << score << std::endl;
        this -> generator();
    }

    void set_speed(int value)
    {
        _speed = value;
    }

    int get_speed()
    {
        return _speed;
    }


////////////////////////////////////////////////////////////////
//////////////////////////////////////////// class controls ///
//////////////////////////////////////////////////////////////

    game_map() : _size_map(nullptr), _x_size(0), _y_size(0), _z_size(0) {}

    game_map(unsigned char x_size, unsigned char y_size, unsigned char z_size)
    {
        _size_map = new unsigned char**[x_size];
        tmp_map = new unsigned char**[x_size];
        for (unsigned char i = 0; i < x_size; i++)
        {
            _size_map[i] = new unsigned char*[y_size];
            tmp_map[i] = new unsigned char*[y_size];

            for (unsigned char j = 0; j < y_size; j++)
            {
                _size_map[i][j] = new unsigned char[z_size];
                tmp_map[i][j] = new unsigned char[z_size];
            }
        }
        _x_size = x_size;
        _y_size = y_size;
        _z_size = z_size;
    }

    game_map(const game_map& that) : _size_map(that._size_map), _x_size(that._x_size), _y_size(that._y_size), _z_size(that._z_size) {}

    game_map& operator = (const game_map& that)
    {
        _size_map = that._size_map;
        _x_size = that._x_size;
        _y_size = that._y_size;
        _z_size = that._z_size;
        return *this;
    }

    ~game_map()
    {
        for (unsigned char i = 0; i < _x_size; i++)
        {
            for (unsigned char j = 0; j < _y_size; j++)
            {
                delete(_size_map[i][j]);
                delete(tmp_map[i][j]);
            }

            delete(_size_map[i]);
            delete(tmp_map[i]);
        }
        delete(_size_map);
        delete(tmp_map);
    }
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////// variables ///
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static GLfloat window_width;
static GLfloat window_height;

extern int old_spd;
extern game_map* Map;
extern unsigned int unitary_size;
extern GLfloat x_map;
extern GLfloat y_map;
extern GLfloat z_map;
extern bool Rotate;
extern bool last_output;
#endif // _CONFIG_H
