#ifndef _RENDER_H
#define _RENDER_H 1

void preparation();
void render();
void timer(int value);

#endif // _RENDER_H
