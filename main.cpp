#include "config.hpp"
#include "accessory.hpp"
#include "render.h"

void input_handler(unsigned char key, int x, int y)
{
    if ((key == 112) && !last_output)
    {
        Rotate = !Rotate;
    }
    if ((key == 32) && !Rotate)
        Map -> set_speed(10);
    glutPostRedisplay();
}

void input_special_handler(int key, int x, int y)
{
    if (!Rotate)
    {
        if(key == GLUT_KEY_UP)
            Map -> data_handler(1);

        if(key == GLUT_KEY_RIGHT)
            Map -> data_handler(2);

        if(key == GLUT_KEY_DOWN)
        {
            Map -> data_handler(3);
        }
        if(key == GLUT_KEY_LEFT)
            Map -> data_handler(4);
    }
    glutPostRedisplay();
}

int main(int argc, char* argv[])
{
    initialization();
    glutInit(&argc, argv);
    glutInitWindowSize(window_width, window_height);
    glutInitWindowPosition(0, 0);

    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH | GLUT_ALPHA);
    glutCreateWindow("Block::Out");
    glutDisplayFunc(render);
    glutReshapeFunc(resize_window);
    glutSpecialFunc(input_special_handler);
    glutKeyboardFunc(input_handler);
    glutCloseFunc(finalization);
    glutTimerFunc(Map -> get_speed(), timer, 1);
    glutCreateMenu(ProcessMenu);
	glutAddMenuEntry("Pause/Continue",1);
    glutAddMenuEntry("Restart",2);
    glutAddMenuEntry("Exit",3);
    glutAttachMenu(GLUT_RIGHT_BUTTON);

    preparation();
    glutMainLoop();
    return EXIT_SUCCESS;
}
